package tutorial.javari;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class JavariController {

    @Autowired
    JavariRepo javariRepo;

    @RequestMapping(value = "/javari", method = RequestMethod.GET)
    @ResponseBody
    public List<Animal> getAllAnimals () throws IOException {
        if(!javariRepo.isAccesed()) {
            javariRepo.readCSVfirstTime();
            javariRepo.setAccesed(true);
            return javariRepo.getMap();
        }
        else if(javariRepo.isAccesed()) {
            return javariRepo.getMap();
        }
        else{
            error(null, "Javari Kosong");
        }

        return javariRepo.getMap();
    }

    @RequestMapping(value = "/javari/{ID}", method = RequestMethod.GET)
    @ResponseBody
    public Animal getAnimal(@PathVariable("ID") String ID) {
        Animal animol = javariRepo.getMap()
                .stream()
                .filter(animal -> Integer.parseInt(ID) == animal.getId())
                .findFirst()
                .orElse(null);

        if(animol != null) {
            return animol;
        }
        else{
            throw new ErrorClass("Not Found");
        }
    }

    @RequestMapping(value = "/javari/{ID}", method = RequestMethod.DELETE)
    @ResponseBody
    public Animal delAnimal(@PathVariable("ID") String ID) throws IOException, ErrorClass {
        Animal deleted = javariRepo.getMap()
                .stream()
                .filter(animal -> Integer.parseInt(ID) == animal.getId())
                .findFirst()
                .orElse(null);
        if(deleted != null) {
            javariRepo.getMap().remove(deleted);
            javariRepo.writeCSV();
            return deleted;
        }
        else{
            throw new ErrorClass("Not Found");
        }
    }

    public Animal error (Animal animal, String msg) {
        throw new ErrorClass(msg);
    }

    @RequestMapping(value = "/javari", method = RequestMethod.POST)
    @ResponseBody
    public void addAnimal(@RequestBody String json) throws IOException {
        HashMap<String, Object> result = new ObjectMapper().readValue(json, HashMap.class);
        Animal check = javariRepo.getMap()
                .stream()
                .filter(animal -> (int)result.get("id") == animal.getId())
                .findFirst()
                .orElse(null);

        if(check == null){
            Animal animal =  new Animal((int)result.get("id")
                    , result.get("type").toString()
                    , result.get("name").toString()
                    , Gender.parseGender(result.get("gender").toString().toLowerCase())
                    , Double.parseDouble(result.get("length").toString())
                    , Double.parseDouble(result.get("weight").toString())
                    , Condition.parseCondition(result.get("condition").toString().toLowerCase()));

            javariRepo.addAnimal(animal);
            javariRepo.writeCSV();
        }
        else{
            throw new ErrorClass("ID sudah ada");
        }



    }





}
