package game;

import java.util.Scanner;


/**
 * Created by billy on 9/27/16.
 * Edited by hafiyyan94 on 4/10/18
 */

public class Main {

    private static final int TOTAL_QUEST = 10;
    private static int totalTimeNeeded;
    private static int points = 100;

    public static void main(String[] args) {
        // write your code here
        Scanner scanner = new Scanner(System.in);
        String startNewQuestsIpt;
        int thresholdTime;
        Fraction expectedAnswer;

        do {
            // initialize value
            startNewQuestsIpt = "";
            QuestionGenerator generator = new QuestionGenerator();

            // Asking for asnwering question threshold time
            System.out.print("How much time do you need "
                    + "to answer each question? (In second) ");
            String rawInput = scanner.nextLine();
            thresholdTime = rawInput.isEmpty() ? 20 : Integer.parseInt(rawInput);

            //Loop for the total number of questions
            for (int questNo = 1; questNo <= TOTAL_QUEST; questNo++) {
                System.out.print(questNo + ") ");
                Thread generatorThread = new Thread(generator);
                Timer stopwatch = new Timer();
                Thread stopwatchThread = new Thread(stopwatch);

                generatorThread.start();

                //Starting the countdown and threshold timer
                stopwatchThread.start();



                // Asking for question
                // And capture before and after the time in milis
                String rawAns = scanner.nextLine();
                int timeNeeded = stopwatch.getThreshold().get();

                // Process user answer
                Fraction userAnswer;
                if (rawAns.contains("/")) {
                    String[] ans = rawAns.split("/");
                    userAnswer = new Fraction(Integer.parseInt(ans[0]),
                            Integer.parseInt(ans[1]));
                } else {
                    userAnswer = new Fraction(Integer.parseInt(rawAns));
                }

                //get the expected Answer from the generator class
                expectedAnswer = generator.getExpectedAnswer();


                // Check answer
                if (expectedAnswer.isEqual(userAnswer)) {

                    if (timeNeeded <= thresholdTime) {
                        points -= stopwatch.getTimer().get();
                        points += stopwatch.calculateInsideThreshold(points);
                        printResult(timeNeeded, points);
                        totalTimeNeeded+= timeNeeded;

                    } else {
                        points -= stopwatch.getTimer().get();
                        points += stopwatch.calculateOutsideThreshold(points);
                        printResult(timeNeeded, points);
                        totalTimeNeeded+= timeNeeded;
                    }
                } else {
                    points -= stopwatch.getTimer().get();
                    printResult(timeNeeded, points);
                    totalTimeNeeded+= timeNeeded;
                }
            }

            System.out.println("\n=========Result==========");
            System.out.println("Your Points: " + points);
            System.out.println("Time needed for all questions: " + totalTimeNeeded);

            // Asking if user want to start a new questions
            // if the respond is not what we want, ask it again and again
            while (!startNewQuestsIpt.equalsIgnoreCase("y")
                    && !startNewQuestsIpt.equalsIgnoreCase("n")) {
                System.out.println("Restart the quiz? [y/n]");
                startNewQuestsIpt = scanner.nextLine();
            }
            System.out.println("\n\n\n\n\n\n");
        } while (startNewQuestsIpt.equalsIgnoreCase("y"));
        // while user input yes, do same step again
    }


    public static void printResult(long timeNeeded, long timeRemaining) {
        System.out.println("\nCurrent Result");
        System.out.println("Score: " + timeRemaining);
        System.out.println("Time Needed: " + timeNeeded);
    }
}
