package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NewYorkPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");

        PizzaStore cabangDepok = new DepokPizzaStore();

        pizza = cabangDepok.orderPizza("cheese");
        System.out.println("Marina ordered a " + pizza + "\n");

        pizza = cabangDepok.orderPizza("clam");
        System.out.println("Marina ordered a " + pizza + "\n");

        pizza = cabangDepok.orderPizza("veggie");
        System.out.println("Marina ordered a " + pizza + "\n");

        pizza = cabangDepok.orderPizza("special");
        System.out.println("Marina ordered a " + pizza + "\n");
    }
}
