package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class RicottaCheese implements Cheese {
    public String toString() {
        return "Ricotta Cheese";
    }
}
