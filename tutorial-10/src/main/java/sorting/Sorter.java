package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    public static int[] quickSort(int[] inputArr) {
        quickSort(inputArr, 0, inputArr.length - 1);
        return inputArr;
    }

    private static void quickSort(int[] inputArr, int start, int end) {
        int startPoint = start;
        int endPoint = end;
        int pivot = inputArr[start + (end - start) / 2];

        while (startPoint <= endPoint) {
            while (startPoint < end && inputArr[startPoint] < pivot) {
                startPoint++;
            }
            while (endPoint > start && inputArr[endPoint] > pivot) {
                endPoint--;
            }
            if (startPoint <= endPoint) {
                swap(inputArr, startPoint, endPoint);
                startPoint++;
                endPoint--;
            }
        }
        swap(inputArr, startPoint, end);
        if (start < endPoint) {
            quickSort(inputArr, start, endPoint);
        }
        if (startPoint < end) {
            quickSort(inputArr, startPoint, end);
        }
    }

    private static void swap(int[] inputArr, int first, int second) {
        int temp = inputArr[first];
        inputArr[first] = inputArr[second];
        inputArr[second] = temp;
    }


}
