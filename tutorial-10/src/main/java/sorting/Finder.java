package sorting;

public class Finder {


    /**
     * Some searching algorithm that possibly the slowest algorithm.
     * This algorithm can search a value irregardless of whether the sequence already sorted or not.
     * @param arrOfInt is a sequence of integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */
    public static int slowSearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;

        for (int element : arrOfInt) {
            if (element == searchedValue) {
                returnValue = element;
            }
        }

        return returnValue;
    }

    public static int binarySearch(int[] arrayOfInt, int searchInt) {
        return binarySearchHelp(arrayOfInt, searchInt, 0, arrayOfInt.length - 1);

    }

    private static int binarySearchHelp(int[] arrayOfInt, int theSearchInt, int start, int finish) {
        if (finish >= 1) {
            int mid = start + (finish - start) / 2;

            if (arrayOfInt[mid] == theSearchInt) {
                return theSearchInt;
            } else if (arrayOfInt[mid] > theSearchInt) {
                return binarySearchHelp(arrayOfInt, theSearchInt, start, mid - 1);
            }

            return binarySearchHelp(arrayOfInt, theSearchInt, mid + 1, finish);
        }
        return -1;
    }

}
