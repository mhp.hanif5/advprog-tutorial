package sorting;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SortAndSearchTest {
    private static long oneSecondInNano;
    private int[] array1Tester;
    private int[] array2Tester;
    private int searchInt;

    @Before
    public void setUp() throws IOException {
        oneSecondInNano = 100000000;
        array1Tester = Main.convertInputFileToArray();
        array2Tester = array1Tester.clone();
        searchInt = 2127;
    }

    @Test
    public void testMainMethodWorks() {
        Assert.assertTrue(mainChecker());
    }

    private boolean mainChecker() {

        boolean success = true;
        try {
            Main.main(null);
        } catch (Exception e) {
            success = false;
        } finally {
            return success;
        }
    }

    @Test
    public void testAllSorter() {
        Assert.assertTrue(isSort(Sorter.slowSort(array1Tester)));
        Assert.assertTrue(isSort(Sorter.quickSort(array2Tester)));
    }

    private boolean isSort(int[] arrayInt) {
        if (arrayInt.length <= 1) {
            return true;
        }
        for (int i = 1; i < arrayInt.length; i++) {
            if (arrayInt[i - 1] > arrayInt[i]) {
                return false;
            }
        }
        return true;
    }

    @Test
    public void testQuickSortFaster() {
        for (int i = 0; i < 20; i++) {
            Sorter.slowSort(array1Tester);
        }
        long timeforSlowSort = System.nanoTime();
        for (int j = 0; j < 100; j++) {
            Sorter.slowSort(array1Tester);
        }
        timeforSlowSort = (System.nanoTime() - timeforSlowSort) / 100;

        for (int i = 0; i < 20; i++) {
            Sorter.quickSort(array2Tester);
        }
        long timeForQuickSort = System.nanoTime();
        for (int i = 0; i < 100; i++) {
            Sorter.quickSort(array2Tester);
        }
        timeForQuickSort = (System.nanoTime() - timeForQuickSort) / 100;

        Assert.assertTrue(timeForQuickSort - timeforSlowSort
                <= -oneSecondInNano);
    }


    @Test
    public void testEachSearchWorks() {
        Assert.assertEquals(searchInt, Finder.slowSearch(array1Tester, searchInt));
        int[] arrayIntSort = Sorter.quickSort(array1Tester);
        Assert.assertEquals(searchInt, Finder.binarySearch(arrayIntSort, searchInt));
    }

    @Test
    public void testBinarySearchFaster() {
        int[] arraySortInt = Sorter.quickSort(array1Tester);
        for (int i = 0; i < 10000; i++) {
            Finder.slowSearch(arraySortInt, searchInt);
        }

        long timeForslowSearch = System.nanoTime();
        for (int j = 0; j < 10000; j++) {
            Finder.slowSearch(arraySortInt, searchInt);
        }

        timeForslowSearch = (System.nanoTime() - timeForslowSearch) / 10000;
        for (int j = 0; j < 1000; j++) {
            Finder.binarySearch(arraySortInt, searchInt);
        }

        long timeForBinarySearch = System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            Finder.binarySearch(arraySortInt, searchInt);
        }
        timeForBinarySearch = (System.nanoTime() - timeForBinarySearch) / 10000;

        Assert.assertTrue(timeForBinarySearch - timeForslowSearch
                <= -oneSecondInNano / 10000);

    }


}
